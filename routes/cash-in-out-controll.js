/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------


router.get("/cash-in-post", (req, res) => {
	util.log("Go Input 계좌 입금!");	
	
	res.render("bizlogic/cash-in-post",
	{
		SubAccountNo: 	"",
		InAmt: 			0,
		CodeAccount: 	"",
		NameAccount: 	"",
		CodeBrief: 		"",
		NameBrief: 		"",
		message:		"",
	});

});

//--- 입금 정보입력 로직
router.post("/cash-in-post", (req, res) => {
	util.log("계좌 입금 Process");
	let body = req.body;

	axios.post(__BIZ_API_URI+"/api/action/in",
		{
    		SubAccountNo: 	body.SubAccountNo,
			InAmt: 			body.InAmt,
			CodeAccount: 	body.CodeAccount,
			NameAccount: 	body.NameAccount,
			CodeBrief: 		body.CodeBrief,
			NameBrief: 		body.NameBrief
		},
		{
			headers: {
				'content-type': 'application/json',
				'accept': 'application/json'
			}
		})
	.then((ret) => {
		util.log(ret);
		res.render("bizlogic/cash-in-post",
		{
 			AfterDeposit:         		ret.data.AfterDeposit,
			BeforeDeposit:  			ret.data.BeforeDeposit,
			CodeAccount:         		ret.data.CodeAccount,
			CodeBrief:         			ret.data.CodeBrief,
			InOutAmt:         			ret.data.InOutAmt,
			InOutFlag:        			ret.data.InOutFlag,
			InOutNo:         			ret.data.InOutNo,
			NameAccount:         		ret.data.NameAccount,
			NameBrief:         			ret.data.NameBrief,
			OverDuePenaltyRefundAmt:	ret.data.OverDuePenaltyRefundAmt,
			ReceivableAccruedAmt:       ret.data.ReceivableAccruedAmt,
			ReceivableRepayAmt:        	ret.data.ReceivableRepayAmt,
			SubAccountNo:         		ret.data.SubAccountNo,
			TradeDate:        			ret.data.TradeDate,
			InAmt:						ret.data.InOutAmt,
			message:					"처리가 끝났습니다"
		});
	})
	.catch((error) => {
		console.error(error);
		res.redirect("/cash-in-post");
	});	
});
//----------------

//--- 출금 정보입력 로직		
router.get("/cash-out-post", (req, res) => {
	util.log("계좌출금 Process");	

	res.render("bizlogic/cash-out-post",
	{
		SubAccountNo: 	"",
		Password: 		"",        
		OutAmt: 		0,        
		CodeAccount: 	"",
		NameAccount: 	"",
		CodeBrief: 		"",
		NameBrief: 		"",
		message:		""
	});
});

//--- 개별 계좌 정보입력 로직
router.post("/cash-out-post", (req, res) => {
	util.log("개별계좌 개설 Process");
	let body = req.body;
	
	axios.post(__BIZ_API_URI+"/api/action/out",
	{
		SubAccountNo: 	body.SubAccountNo,
		Password: 		body.Password,        
		OutAmt: 		body.OutAmt,        
		CodeAccount: 	body.CodeAccount,
		NameAccount: 	body.NameAccount,
		CodeBrief: 		body.CodeBrief,
		NameBrief: 		body.NameBrief
	},	
	{
		headers: {
			'content-type': 'application/json',
			'accept': 'application/json'
		}
	})
	.then((ret) => {
		util.log(ret);
		util.log(ret.data);
		res.render("bizlogic/cash-out-post",
		{
			AfterDeposit:         		ret.data.AfterDeposit,
			BeforeDeposit:  			ret.data.BeforeDeposit,
			CodeAccount:         		ret.data.CodeAccount,
			CodeBrief:         			ret.data.CodeBrief,
			InOutAmt:         			ret.data.InOutAmt,
			InOutFlag:        			ret.data.InOutFlag,
			InOutNo:         			ret.data.InOutNo,
			NameAccount:         		ret.data.NameAccount,
			NameBrief:         			ret.data.NameBrief,
			OverDuePenaltyRefundAmt:	ret.data.OverDuePenaltyRefundAmt,
			ReceivableAccruedAmt:       ret.data.ReceivableAccruedAmt,
			ReceivableRepayAmt:        	ret.data.ReceivableRepayAmt,
			SubAccountNo:         		ret.data.SubAccountNo,
			TradeDate:        			ret.data.TradeDate,
			OutAmt:         			ret.data.InOutAmt,
			Password:                   "0000",
			message:					"처리가 끝났습니다"

		});
	})
	.catch((error) => {
		console.error(error);	
		res.redirect("/cash-out-post");
	});	
});
//----------------

module.exports = router;
