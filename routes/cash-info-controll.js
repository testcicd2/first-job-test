/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------

router.get("/", (req, res) => {
	res.redirect("cash-deposit-info");
});



//--- 현금 잔고 가져오기 
router.get("/cash-deposit-info", (req, res) => {
	util.log("Cash deposit info GET page!");	
	res.render("bizlogic/cash-deposit-info", {
		mode: "init",
		SubAccountNo : req.SubAccountNo,
		message:		""
	}); 	
});

router.post("/cash-deposit-info", (req, res) => {
    util.log("현금 잔고 정보 가져오기");
  	let body = req.body;

	axios.get(__BIZ_API_URI+"/api/cashdeposit/info",
	{
        params: {
            SubAccountNo : body.SubAccountNo
		}
	},
	{
		headers: {
			'content-type': 'application/json'
		}
	})
	.then((ret) => {
		util.log("SUCCESS!");
		util.log(ret);
		util.log(ret.data);
		util.log("계좌번호 "+ ret.data.SubAccountNo);
		
        res.render("bizlogic/cash-deposit-info", {
			mode: "view",
			SubAccountNo  : ret.data.SubAccountNo,
			DepositCash   : ret.data.DepositCash,
			LoanAmt  : ret.data.LoanAmt,
			MarginAmt  : ret.data.MarginAmt,
			ReceivableAmt : ret.data.ReceivableAmt,
			message: "조회가 끝났습니다"
       });
    })
	.catch(function (error) {
		  console.log("data=" , error.response.data);
		  console.log("status=" , error.response.status);
		  console.log("ErrMsg=" , error.response.data.ErrMsg);
		  console.log('Error=', error.message);
		res.render("bizlogic/cash-deposit-info", {
			mode: "init",
			SubAccountNo : req.SubAccountNo,
			message:		error.response.data.ErrMsg
		});
	  });
});
//--------------
//--- 출납 내역 정보 가져오기 
router.get("/cash-in-out-info", (req, res) => {
	util.log("Cash In Out Trading List GET page!");	
	res.render("bizlogic/cash-in-out-info", {
		mode: "init",
		SubAccountNo : "",
		TradeDate: "",
		message:		""

	});   
});

router.post("/cash-in-out-info", (req, res) => {
	util.log("출납 내역 정보 가져오기");
	let body = req.body;
	util.log("req=>"+__BIZ_API_URI+"/api/cashdeposit/inout_list");
	util.log("SubAccountNo:"+body.SubAccountNo +",tradeDate:"+body.TradeDate);
	
	axios.get(__BIZ_API_URI+"/api/cashdeposit/inout_list",
	{
        params: {
            SubAccountNo : body.SubAccountNo,
			TradeDate : body.TradeDate
		}
    },	
	{
		headers: {
			'content-type': 'application/json',
			'accept': 'application/json'
		}
	})
 	.then((ret) => {
		util.log("개별 계좌 조회 !");
		util.log(ret);
		util.log(ret.data);

		res.render("bizlogic/cash-in-out-info", {
			mode: "view",
            SubAccountNo : body.SubAccountNo,
			TradeDate : body.TradeDate,
			list: ret.data.ListInOutBook,
			message: "조회가 끝났습니다"
		});
	})
	.catch((error) => {
		console.log("data=" 	, error.response.data);
		console.log("status=" 	, error.response.status);
		console.log("ErrMsg=" 	, error.response.data.ErrMsg);
		console.log('Error='	, error.message);
	  res.render("bizlogic/cash-in-out-info", {
		  mode			: 	"init",
		  SubAccountNo 	: 	req.SubAccountNo,
		  TradeDate		: 	req.TradeDate,
		  message		:	error.response.data.ErrMsg
	  });
	});	    
});	


module.exports = router;
